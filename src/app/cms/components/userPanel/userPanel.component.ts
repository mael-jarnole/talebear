import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange} from '@angular/core';
import {AppService} from '../../../commons/services/app.service';
import {User, UserService} from '../../../commons/services/user.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {ChangePasswordModalComponent} from '../change-password-modal/change-password-modal.component';
import {MessageComponent} from '../../../commons/components/message/message.component';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit, OnChanges {

  roles: string[];
  @Input('user') user: User;
  @Output() onUserModification = new EventEmitter<User>();
  userForm: FormGroup;

  // for avatar upload
  uploadedFiles = [];
  uploadError: Error;
  currentStatus: number;

  readonly STATUS_INITIAL = 0;
  readonly STATUS_SAVING = 1;
  readonly STATUS_SUCCESS = 2;
  readonly STATUS_FAILED = 3;

  constructor(private App: AppService, private userService: UserService,
              private el: ElementRef, private fb: FormBuilder, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.App.getConf().subscribe((res: any) => {
      this.roles = res.roles;
    });
  }

  changePassword(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    const ref = this.dialog.open(ChangePasswordModalComponent, {data: this.user._id});
    ref.afterClosed().subscribe((res) => {
      if (res) {
        this.dialog.open(MessageComponent, {data: res});
      }
    });
  }

  rebootPassword(event: Event) {
    this.userService.resetPassword(this.user._id).subscribe((result) => {
      console.log('Mot de passe reset OK');
      console.log(result);
    }, (error) => {
      console.log('Une erreur est survenue' + error);
    });
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes.hasOwnProperty('user')) {
      this.user.avatar = this.user.avatar ? this.user.avatar : '';
      this.userForm = this.fb.group(this.user);
    }
  }

  /**
   * @param user
   */
  addUser(user: User) {
    this.userService.putUser(user).subscribe((result: User) => {
      this.dialog.open(MessageComponent, {data: 'Utilisateur modifié/ajouté: ' + result.name});
      this.onUserModification.emit(result);
    }, (error) => {
      this.dialog.open(MessageComponent, {data: 'Une erreur est survenue: \n' + error});
    });
  }

  resetUpload() {
    this.userForm.get('avatar').enable();
    this.currentStatus = this.STATUS_INITIAL;
    this.uploadedFiles = [];
    this.uploadError = null;
  }

  save(formData: FormData) {
    // upload data to the server
    this.currentStatus = this.STATUS_SAVING;
    this.userForm.get('avatar').disable();
    this.userService.uploadAvatar(formData)
      .subscribe((x: any) => {
        this.currentStatus = this.STATUS_SUCCESS;
        this.uploadedFiles.push(x.avatar);
        this.userForm.patchValue({'avatar': x.avatar});
        this.userForm.get('avatar').enable();
      }, err => {
        this.uploadError = err;
        this.currentStatus = this.STATUS_FAILED;
        this.userForm.get('avatar').enable();
      });
  }

  avatarFileChange() {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#Avatar');
    const fileCount: number = inputEl.files.length;
    if (fileCount) {
      const formData = new FormData();
      formData.append('avatar', inputEl.files.item(0));
      // save it
      this.save(formData);
    } else {
      this.dialog.open(MessageComponent, {data: 'erreur, pas de fichier'});
    }
  }

}
