import { Injectable } from '@angular/core';

@Injectable()
export class SortingService {

  constructor() { }

  public sortByDateAscending(array: object[], dateField: string) {
    this.sortByDate(array, dateField, true);
  }

  public sortByDateDescending(array: object[], dateField: string) {
    this.sortByDate(array, dateField, false);
  }

  private sortByDate(array: object[], dateField: string, order: boolean) {
    if (array[0].hasOwnProperty(dateField)) {
      array.sort(function(a, b){
        return +new Date(b[dateField]) - +new Date(a[dateField]);
      });
      if (order) {
        array.reverse();
      }
    } else {
      throw new ReferenceError('The provided array does not have a field: ' + dateField);
    }
  }
}
