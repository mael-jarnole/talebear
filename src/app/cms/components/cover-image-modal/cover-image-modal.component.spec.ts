import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverImageModalComponent } from './cover-image-modal.component';

describe('CoverImageModalComponent', () => {
  let component: CoverImageModalComponent;
  let fixture: ComponentFixture<CoverImageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverImageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverImageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
