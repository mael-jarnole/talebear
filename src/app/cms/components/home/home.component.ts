/**
 * Created by Maël on 06/04/2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Title} from '@angular/platform-browser';
import { User } from '../../../commons/services/user.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit {
  private title: string;
  currentUser: User;

  ngOnInit () {
    this.route.data.subscribe((data: { user: User }) => {
      this.currentUser = data.user;
    }, error => {
      window.alert(JSON.stringify(error));
    });
  }

  constructor(
              private Title: Title, private route: ActivatedRoute
  ) {
    this.title = 'Talebearer - Homepage';
    Title.setTitle(this.title);
  }

}

