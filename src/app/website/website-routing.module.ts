// app.routes.ts
import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {UserResolver} from '../commons/services/user-resolver.service';
import {PostResolverService} from '../commons/services/post-resolver.service';
import {WebsiteComponent} from './website.component';
import {ArticleTilesComponent} from './components/article-tiles/article-tiles.component';
import {ArticleViewerComponent} from "./components/article-viewer/article-viewer.component";

const AppRoutes: Routes = [
  {
    path: 'webzine',
    component: WebsiteComponent,
    children: [
      {
        path: '',
        component: ArticleTilesComponent,
        resolve: {posts : PostResolverService}
      },
      {
        path: 'article/:id',
        component: ArticleViewerComponent,
        resolve: {posts : PostResolverService}
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(AppRoutes),
  ],
  exports: [
    RouterModule
  ],
  providers: [
    UserResolver,
    PostResolverService
  ]
})
export class WebsiteRoutingModule {
}
