import {LOCALE_ID, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {Title} from '@angular/platform-browser';
import {CmsComponent} from './cms.component';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {HomeComponent} from './components/home/home.component';
import {AuthenticatorService} from '../commons/services/authenticator.service';
import {UserService} from '../commons/services/user.service';
import {UserManagementComponent} from './components/userManagement/userManagement.component';
import {EditorComponent} from './components/editor/editor.component';
import {CheckerComponent} from './components/checker/checker.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppService} from '../commons/services/app.service';
import {PostService} from '../commons/services/post.service';
import {FileUploadService} from '../commons/services/upload.service';
import {CmsRoutingModule} from './cms-routing.module';
import {UserPanelComponent} from './components/userPanel/userPanel.component';
import {BundledMaterialModule} from '../cots/module/material.module';
import {NewUserModalComponent} from './components/newUserModal/new-user-modal/new-user-modal.component';
import {SystemMessageModalComponent} from './components/systemMessageModal/system-message-modal/system-message-modal.component';
import {PublisherComponent} from './components/publisher/publisher.component';
import {DatePickingModalComponent} from './components/date-picking-modal/date-picking-modal.component';
import {DatePipe} from '@angular/common';
import {TinymceComponent} from './components/tinymce/tinymce.component';
import {SortingService} from '../commons/services/sorting.service';
import {UserBlockComponent} from './components/user-block/user-block.component';
import {CoverImageModalComponent} from './components/cover-image-modal/cover-image-modal.component';
import {ChangePasswordModalComponent} from './components/change-password-modal/change-password-modal.component';

@NgModule({
  declarations: [
    CmsComponent,
    LoginComponent,
    ProfileComponent,
    HomeComponent,
    UserManagementComponent,
    EditorComponent,
    CheckerComponent,
    UserPanelComponent,
    NewUserModalComponent,
    SystemMessageModalComponent,
    PublisherComponent,
    DatePickingModalComponent,
    UserBlockComponent,
    TinymceComponent,
    CoverImageModalComponent,
    ChangePasswordModalComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CmsRoutingModule,
    BundledMaterialModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    AuthenticatorService, Title, UserService, AppService, FileUploadService, PostService, DatePipe,
    SortingService
  ],
  exports: [
    UserBlockComponent
  ],
  entryComponents: [
    NewUserModalComponent,
    SystemMessageModalComponent,
    DatePickingModalComponent,
    CoverImageModalComponent,
    ChangePasswordModalComponent
  ]
})
export class CmsModule {
}
