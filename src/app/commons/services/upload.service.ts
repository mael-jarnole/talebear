import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class FileUploadService {

  baseUrl = environment.baseUrl;

  constructor(private _http: HttpClient) { }

  upload(formData) {
    const url = `${this.baseUrl}/upload/`;
    return this._http.post(url, formData)
      .map((x: any[]) => x
        // add a new field url to be used in UI later
          .map(item => Object
            .assign({}, item, { url: `${this.baseUrl}/images/${item.id}` }))
      );
  }
}
