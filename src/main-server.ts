/**
 * Created by Maël on 04/09/2017.
 */
import {enableProdMode} from "@angular/core";
export { AppServerModule } from './app/app-server.module';

enableProdMode();
