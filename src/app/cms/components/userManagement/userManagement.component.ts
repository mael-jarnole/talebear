/**
 * Created by Maël on 06/04/2017.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User, UserService} from '../../../commons/services/user.service';
import {Title} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/';
import {NewUserModalComponent} from '../../components/newUserModal/new-user-modal/new-user-modal.component';
import {MessageComponent} from '../../../commons/components/message/message.component';

@Component({
  selector: 'app-user-management',
  templateUrl: './userManagement.component.html',
  styleUrls: ['./userManagement.component.css']
})
export class UserManagementComponent implements OnInit {
  private title: string;
  currentUser: User;
  users: User[];
  userPanelShow: boolean;
  selectedUser: User;

  ngOnInit() {

    this.route.data.subscribe((data: { user: User }) => {
      this.currentUser = data.user;
    }, this.errorHandler);
    this.userService.getAllUsers().subscribe(users => {
      this.users = users;
    }, this.errorHandler);
    this.userPanelShow = false;
    this.selectedUser = null;
  }

  constructor(private  userService: UserService, private dialog: MatDialog,
              private route: ActivatedRoute, private titleService: Title) {
    this.title = 'Meta 9 - Gestion Utilisateur';
    titleService.setTitle(this.title);
  }

  /**
   * @description Deletes an user
   * @param user
   */
  deleteUser(user: User) {
    if (user.email === this.currentUser.email) {
      window.alert('Impossible de supprimer votre propre compte.');
    } else {
      const yes = window.confirm('Voulez-vous supprimer ' + user.name + ' ?');
      if (yes) {
        this.userService.deleteUser(user.email).subscribe((result) => {
          this.users = this.users.filter(function (element) {
            return element.email !== user.email;
          });
          window.confirm('Utilisateur effacé ' + user.name);
          this.userPanelShow = false;
        }, (error) => {
          window.alert('L\'erreur suivante est survenue : \n' + error.toString());
        });
      }
    }
  }

  triggerUserPanel(user) {
    this.selectedUser = user;
    this.userPanelShow = true;
  }

  private errorHandler(error) {
    window.alert(JSON.stringify(error));
  }

  reloadUsers() {
    this.userService.getAllUsers().subscribe(users => {
      this.users = users;
    }, this.errorHandler);
  }

  openDialog() {
    const modal = this.dialog.open(NewUserModalComponent);
    modal.afterClosed().subscribe(() => {
      this.reloadUsers();
    }, error => {
      this.dialog.open(MessageComponent,  {data: JSON.stringify(error)});
    });
  }
}

