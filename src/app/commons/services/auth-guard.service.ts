/**
 * Created by Maël on 02/07/2017.
 */
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticatorService} from './authenticator.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor (private auth: AuthenticatorService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    if ( this.auth.isLoggedIn() ) {
      return true;
    } else {
      this.auth.redirectUrl = url;
      console.log('navigating to : /login');
      this.router.navigate(['/login']).then(res => {
        console.log(res);
      }, (error) => {
        console.log(error);
      });
      return false;
    }
  }
}
