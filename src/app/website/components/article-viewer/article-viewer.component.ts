import { Component, OnInit } from '@angular/core';
import {Post} from '../../../commons/services/post.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-article-viewer',
  templateUrl: './article-viewer.component.html',
  styleUrls: ['./article-viewer.component.scss']
})
export class ArticleViewerComponent implements OnInit {

  posts: Post[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((data: { posts }) => {
      this.posts = data.posts;
    }, error => {
      window.alert(JSON.stringify(error));
    });
  }

}
