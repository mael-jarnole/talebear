// dependencies
const mongoose = require('mongoose');
const conf = require('../conf');
const user = require('../models/user');
const prompt = require('prompt');

function createAdminUser () {
  var admin = new user();
  var schema = {
    properties: {
      email: {
        required: true
      },
      password: {
        hidden: true,
        replace: '*',
        required: true
      }
    }
  };
  //
  admin.name = "Administrator";
  admin.role = "Admin"
  prompt.start();
  prompt.get(schema, function (err, result) {
    console.info('Command-line input received:');
    console.log('  email: ' + result.email);
    console.log('  password: ' + '***hidden***');
    admin.email = result.email;
    admin.setPassword(result.password);
    admin.save(function(err) {
      if(err){
        /**
         * TODO manage already existing email, already existing user collection
         * TODO (OPTIONS : dropping user collection, renaming user collection for this app(user_talebearer ?), keeping it and choose a different user email for login)
         */
        console.error('creation of admin failed'+err);
      }
      else {
        console.info('[SUCCESS] Admin user was created');
      }
    });
  });
}

mongoose.Promise = global.Promise;

const promise = mongoose.connect("mongodb://"+conf.db_url, {useMongoClient: true});

promise.then(function(db) {
    console.info('Database is running on '+ conf.db_url);
    user.findOne({ role : "Admin" }, function(err,result){
      if(err){
        console.error('Something went wrong when querying for Admin user');
      } else {
        if (!result) {
          console.info('Admin user not found, let\'s create one...');
          createAdminUser();
        } else {
          console.info('Admin user found...Rolling on');
        }
      }
    });
}, function(error) {
  console.error(error); //
});



module.exports = mongoose;
