/**
 * Created by Maël on 02/07/2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.html',
  styleUrls: ['./not-found.css']
})
export class PageNotFoundComponent {

}
