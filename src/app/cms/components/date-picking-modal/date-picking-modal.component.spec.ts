import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePickingModalComponent } from './date-picking-modal.component';

describe('DatePickingModalComponent', () => {
  let component: DatePickingModalComponent;
  let fixture: ComponentFixture<DatePickingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatePickingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
