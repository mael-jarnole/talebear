// user.service.ts
import {Injectable} from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';

export class User {
  _id: string;
  email: string;
  name: string;
  avatar: string;
  role: string;
  facebook: string;
  twitter: string;
  instagram: string;
  website: string;
  log: string;
  private password: string;

  constructor(name?: string, email?: string) {
    this.email = email ? email : '';
    this.name = name ? name : '';
    this.avatar = '';
    this.role = '';
    this.log = '';
    this.facebook = '';
    this.twitter = '';
    this.instagram = '';
    this.website = '';
  }

  public setPassword(password) {
    this.password = password;
  }
}

@Injectable()
export class UserService {
  private loggedIn = false;
  private currentUser: User;

  constructor(private http: HttpClient) {
    this.loggedIn = !!localStorage.getItem('auth_token');
  }

  getCurrentUserInfo() {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this.http.get(environment.baseUrl + '/api/user', {headers}).map((res: User) => {
      this.currentUser = res;
      return res;
    });
  }

  getAllUsers(): Observable<User[]> {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this.http.get(environment.baseUrl + '/api/user/all', {headers})
      .map((res: User[]) => {
        return res;
      });
  }

  uploadAvatar(formData) {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this.http.post(environment.baseUrl + '/upload/avatar', formData, {headers});
  }

  deleteUser(email: String): Observable<User> {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    headers.append('email', email.toString());
    return this.http.delete(environment.baseUrl + '/api/user', {headers})
      .map((res: User) => {
        return res;
      });
  }

  putUser(user: User, password?: string): Observable<User> {
    const headers = new HttpHeaders();
    if (password) {
      user.setPassword(password);
    }
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this.http.put(environment.baseUrl + '/api/user', JSON.stringify(user), {headers})
      .map((res: User) => {
        return res;
      });
  }

  getCurrentUser(): User {
    return this.currentUser;
  }

  changePasswordById(UserPasswordInfo: { old: string, new: string, userId: string }) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this.http.patch(environment.baseUrl + '/api/user/' +
      UserPasswordInfo.userId + '/password', JSON.stringify(UserPasswordInfo), {headers});
  }

  resetPassword(userId) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this.http.patch(environment.baseUrl + '/api/user/' +
      userId + '/password', {password: 'changeme'}, {headers});
  }
}
