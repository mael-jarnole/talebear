import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemMessageModalComponent } from './system-message-modal.component';

describe('SystemMessageModalComponent', () => {
  let component: SystemMessageModalComponent;
  let fixture: ComponentFixture<SystemMessageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemMessageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemMessageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
