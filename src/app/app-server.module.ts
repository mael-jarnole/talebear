/**
 * Created by Maël on 04/09/2017.
 */
import {NgModule} from "@angular/core";
import {ServerModule} from "@angular/platform-server";

import {AppModule} from "./app.module";
import {AppComponent} from "./app.component";

@NgModule({
  imports: [
    AppModule,
    ServerModule
  ],
  bootstrap: [AppComponent]
})
export class AppServerModule {}
