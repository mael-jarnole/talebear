import {Component, OnInit} from '@angular/core';
import {User, UserService} from '../../../../commons/services/user.service';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-new-user-modal',
  templateUrl: './new-user-modal.component.html',
  styleUrls: ['./new-user-modal.component.css']
})
export class NewUserModalComponent implements OnInit {

  model = {name: '', email: '', password: '', confirm: ''};
  constructor(private userService: UserService, private ref: MatDialogRef<NewUserModalComponent>) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.model.password && this.model.password === this.model.confirm) {
      this.userService.putUser(new User(this.model.name, this.model.email),  this.model.password).subscribe(
        () => {
          this.ref.close();
        }, error => {
          this.ref.close(error);
        })
    }
  }

}
