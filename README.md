# Talebear

A CMS created for META-9 Website (an Art & Comic related media site).
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Configuration information

The server-side code requires a few configuration files, including a `.env` file in the root directory
containing the following :
```dosini
CLOUD_NAME=your cloud name for cloudinary support
API_KEY=your cloudinary api key,
API_SECRET=your cloudinary api secret
MONGODB=your mongodb url
SECRET=a secret for authentication with JWT
DOMAIN=your domain name, just a useful information to have
```

## Development server

Run `ng serve` for a dev server (frontend). Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Run `npm run server` for a dev server (backend). The server listens to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
