import {Component, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {Post, PostService} from '../../../commons/services/post.service';
import {MatDialog} from '@angular/material';
import {DatePickingModalComponent} from '../date-picking-modal/date-picking-modal.component';
import {SortingService} from '../../../commons/services/sorting.service';
import {environment} from '../../../../environments/environment';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {MessageComponent} from '../../../commons/components/message/message.component';

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.css']
})
export class PublisherComponent implements OnInit {

  posts: Post[];
  links;
  total: number;
  currentPage: number;

  constructor(private route: ActivatedRoute, private dialog: MatDialog, private postService: PostService,
  private dateFilter: DatePipe, private sort: SortingService, private _http: HttpClient) { }

  ngOnInit() {
    this.route.data.subscribe((data: { posts }) => {
      this.posts = data.posts.post;
      this.links = data.posts._links;
      this.total = data.posts.total;
      this.currentPage = data.posts.currentPage;
      this.sort.sortByDateAscending(this.posts, 'posted_on');
    }, error => {
      window.alert(JSON.stringify(error));
    });
  }

  publishState(post) {
    return  post.posted_on ?  'Sortie le ' +
      this.dateFilter.transform(post.posted_on, 'fullDate') : 'pas de date sortie...';
  }

  loadPageContent($event) {
    const next = this.currentPage < $event.pageIndex;
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    this._http.get(environment.baseUrl + (next ? this.links.next.href : this.links.previous.href), {headers})
      .subscribe((posts: any) => {
      this.posts = posts.post;
      this.links = posts._links;
      this.total = posts.total;
      this.currentPage = posts.currentPage;
      this.sort.sortByDateAscending(this.posts, 'posted_on');
    });
  }

  setReleaseDate (post: Post) {
    const modal = this.dialog.open(DatePickingModalComponent, {data: {post: post}});
    modal.afterClosed().subscribe(() => {
        this.postService.getPosts().subscribe(posts => {
          this.posts = posts.post;
          this.links = posts._links;
          this.total = posts.total;
          this.currentPage = posts.currentPage;
          this.sort.sortByDateAscending(this.posts, 'posted_on');
        }, (error) => {
          this.dialog.open(MessageComponent, {data: 'Erreur: \n' + error});
        });
      }, error => {
      this.dialog.open(MessageComponent, {data: 'Erreur: \n' + error});
    });
  }
}
