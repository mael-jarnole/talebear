// login.component.ts
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AuthenticatorService} from '../../../commons/services/authenticator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.style.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  fetching: boolean;
  roles: string[];

  constructor(private auth: AuthenticatorService, private router: Router, private Title: Title, private fB: FormBuilder) {
    this.fetching = false;
  }

  ngOnInit() {
    this.Title.setTitle('Se connecter - Talebearer');
    this.form = this.fB.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit(login, password) {
    this.fetching = true;
    this.auth.login(login, password).subscribe((res) => {
      if (res.token) {
        const redirect = this.auth.redirectUrl ? this.auth.redirectUrl : '/cms';
        this.router.navigate([redirect]).then();
      } else {
        this.fetching = false;
        this.form.reset();
        this.form.setErrors({error: 'Erreur inconnue, le token d\'authentification n\'existe pas'});
      }
    }, (err) => {
      this.fetching = false;
      this.form.reset();
      this.form.setErrors({error: err._body.message});
    });
  }
}
