import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleTilesComponent } from './article-tiles.component';

describe('ArticleTilesComponent', () => {
  let component: ArticleTilesComponent;
  let fixture: ComponentFixture<ArticleTilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleTilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleTilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
