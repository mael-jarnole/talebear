/**
 * Created by Maël on 02/07/2017.
 */
import { NgModule } from '@angular/core';

import {
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatGridListModule,
  MatListModule,
  MatTooltipModule,
  MatChipsModule,
  MatIconModule,
  MatTabsModule,
  MatRippleModule,
  MatSelectModule,
  MatDialogModule,
  MatMenuModule,
  MatSidenavModule,
  MatPaginatorModule,
  MatDatepickerModule,
  MatNativeDateModule, MatProgressBar, MatProgressBarModule
} from '@angular/material';

@NgModule({
  imports: [
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatGridListModule,
    MatListModule,
    MatTooltipModule,
    MatChipsModule,
    MatIconModule,
    MatTabsModule,
    MatRippleModule,
    MatSelectModule,
    MatDialogModule,
    MatMenuModule,
    MatSidenavModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatProgressBarModule
  ],
  exports: [
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatGridListModule,
    MatListModule,
    MatTooltipModule,
    MatChipsModule,
    MatIconModule,
    MatTabsModule,
    MatRippleModule,
    MatSelectModule,
    MatDialogModule,
    MatMenuModule,
    MatSidenavModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatProgressBarModule
]
})
export class BundledMaterialModule { }
