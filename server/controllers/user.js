/**
 * Created by mael on 18/04/17.
 */
const User = require('../models/user');

module.exports.updateUser = function(req, res) {
  User.findOne({_id:req.body._id}, function (err, existingUser){
    if(err){
      res.status(404);
      res.next(err);
    }
    if(existingUser){
      existingUser.name = req.body.name; // TODO Add virtual first name and last name
      existingUser.email = req.body.email;
      // Optional fields
      existingUser.log = req.body.log;
      existingUser.instagram = req.body.instagram;
      existingUser.twitter = req.body.twitter;
      existingUser.avatar = req.body.avatar;
      existingUser.facebook = req.body.facebook;
      existingUser.website = req.body.website;
      existingUser.role = (req.body.role ? req.body.role: existingUser.role);
      existingUser.save(function(err) {
        if (err) {
          res.status(500);
          res.json(err);
        } else {
          res.status(200);
          res.json(existingUser);
        }
      });
    }
    else {
      // create user
      var user = new User({
        name :  req.body.name,
        email : req.body.email
      });
      user.setPassword(req.body.password);
      user.save(function(err) {
        if (err) {
          res.status(500);
          res.json(err);
        } else {
          res.status(200);
          res.json(user);
        }
      });
    }
  });
};

/**
 *
 * @param req
 * @param res
 */
module.exports.changePassword = function(req, res) {
  User.findById(req.params.userId, function (err, existingUser) {
    if(err){
      res.status(500).json(err);
    } else {
      if(!existingUser) {
        res.status(404).json({message: "User not found"})
      }
      if(req.body.password) {
        existingUser.setPassword(req.body.password);
        existingUser.save();
        res.status(202).json({message: "password reset", user: existingUser});
        return;
      }
      if(existingUser.validPassword(req.body.old)){
        existingUser.setPassword(req.body.new);
        existingUser.save();
        res.status(202).json({message: "password changed", user: existingUser});
      } else {
        res.status(401).json(existingUser);
      }
    }
  });
};

module.exports.deleteUser = function(req, res) {
  User.findOneAndRemove({email: req.headers.email}, function(err){
    if(err){
      res.status(404).json(err);
    } else {
      res.status(200).json('{ success : true, message : "User deleted"}');
    }
  });
};

module.exports.getAllUsers = function (req,res) {
  User.find({}, {hash : 0, salt: 0}, function (err, users) {
    if (users){
      res.status(200).json(users);
    }
    if (!users || err){
      res.status(404).json({
        error: 'No User found or there was an error connecting to the database'
      });
    }
  });
}

module.exports.getCurrentUserInfo = function (req,res) {
  User.findOne({email: req.user.email}, {hash: 0, salt: 0}, function (err, user) {
    if (user) {
      res.status(200).json(user);
    }
    if (!user || err) {
      res.status(404).json({
        error: 'User was not found or there was an error connecting to the database'
      });
    }
  });
}
