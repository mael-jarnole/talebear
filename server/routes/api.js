const express = require('express');
const router = express.Router();
const jwt = require('express-jwt');
const conf = require('../conf')
const authCtrl = require('../controllers/authentification');
const cors = require('cors');
const post = require('./post');
const user = require('./user');

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.get('/conf', cors(), (req, res) => {
  res.status(200);
  res.json(conf.frontend);
});

router.options('/login', cors());
router.post('/login', cors(), (req,res) => {
  authCtrl.login(req, res);
});

router.options('/user', cors());
router.use('/user', cors(), user);

router.options('/post', cors());
router.use('/post', cors(), post);

module.exports = router;
