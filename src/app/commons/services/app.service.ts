// user.service.ts
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {environment} from '../../../environments/environment';

@Injectable()
export class AppService {

  private roles: string[];

  constructor(private http: HttpClient) {
  }

  getConf() {
    return this.http.get(environment.baseUrl + '/api/conf')
      .map((res) => {
        return res;
      });
  }

  setRoles(pRoles: string[]) {
    this.roles = pRoles;
  }

  getRoles() {
    return this.roles;
  }

}
