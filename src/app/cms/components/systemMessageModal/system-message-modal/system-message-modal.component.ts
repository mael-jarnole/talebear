import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-system-message-modal',
  templateUrl: './system-message-modal.component.html',
  styleUrls: ['./system-message-modal.component.css']
})
export class SystemMessageModalComponent implements OnInit {

  @Input('message') message: string;

  constructor() {
  }

  ngOnInit() {
  }

}
