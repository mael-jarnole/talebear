import {Component, OnInit} from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {Title} from '@angular/platform-browser';
import {Post, PostService} from '../../../commons/services/post.service';
import {User, UserService} from '../../../commons/services/user.service';
import {environment} from '../../../../environments/environment';
import {SortingService} from '../../../commons/services/sorting.service';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material';
import {MessageComponent} from '../../../commons/components/message/message.component';


@Component({
  selector: 'app-checker',
  templateUrl: './checker.component.html',
  styleUrls: ['./checker.component.css']
})
export class CheckerComponent implements OnInit {
  posts: Post[];
  userNamesMap: { [userId: string]: User } = {};
  currentPost: Post;
  links;
  total: number;
  currentPage: number;

  constructor(private title: Title,
              private postService: PostService, private userService: UserService, private _http: HttpClient,
              private sort: SortingService, private route: ActivatedRoute, private dialog: MatDialog) {
  }

  setPost(post: Post): void {
    this.postService.getPostById(post._id).subscribe(completePost => {
      if (!completePost) {
        this.posts.forEach((el, i, arr) => {
          if (el._id === post._id) {
            arr.splice(i, 1);
          }
        });
        if (!this.posts.length && this.currentPage > 1) {
          this.loadPageContent({pageIndex: this.currentPage - 1});
        }
      }
      this.currentPost = completePost;
    }, err => {
      this.dialog.open(MessageComponent, {data: 'Erreur:\n' + err});
    });
  }

  reloadPost(post: Post) {
    this.setPost(post);
  }

  loadPageContent($event) {
    const next = this.currentPage < $event.pageIndex;
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    this._http.get(environment.baseUrl + (next ? this.links.next.href : this.links.previous.href), {headers})
      .subscribe((posts: any) => {
      this.posts = posts.post;
      this.links = posts._links;
      this.total = posts.total;
      this.currentPage = posts.currentPage;
      this.sort.sortByDateAscending(this.posts, 'posted_on');
    });
  }

  ngOnInit() {
    this.route.data.subscribe((data: { posts }) => {
      this.posts = data.posts.post;
      this.links = data.posts._links;
      this.total = data.posts.total;
      this.currentPage = data.posts.currentPage;
    }, error => {
      window.alert(JSON.stringify(error));
    });
    this.title.setTitle('Relecture d\'articles');
    this.loadUsers();
  }

  private loadUsers() {
    this.userService.getAllUsers().subscribe((users: User[]) => {
      users.forEach(user => {
        this.userNamesMap[user._id] = user;
      }, this);
    }, error => {
      this.dialog.open(MessageComponent,
        {data: 'Une erreur est survenue lors de la recuperation des utilisateurs: \n' + error});
    });
  }
}
