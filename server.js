/**
 * TODO use angular universal for serving the frontend app
 *
 *
 */

// Get dependencies
require('dotenv').config();
const express = require('express');
const path = require('path');
const https = require('https');
const fs = require('fs');
const bodyParser = require('body-parser');
const passport = require('passport');
var helmet = require('helmet')

// Get the routes
const api = require('./server/routes/api');
const upload = require('./server/routes/upload');

require('./server/db/db');

const cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
});

require('./server/provider/passport');

const app = express();

app.use(helmet())

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Use middleware passport
app.use(passport.initialize());

// error handlers
// Catch unauthorised errors
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message": err.name + ": " + err.message});
  }
});

// Set our api routes
app.use('/api', api);
app.use('/upload', upload);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  // Exception : The path for the ssl challenge
 if(req.path.indexOf('.well-known') === -1) {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
 }
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.HTTP_PORT || 80,
  httpsPort = process.env.HTTPS_PORT || 443;

app.set('port', port);

if (fs.existsSync('./sslcert/fullchain.pem') && fs.existsSync('./sslcert/privkey.pem')) {
  // Create https server
  const options = {
    cert: fs.readFileSync('./sslcert/fullchain.pem'),
    key: fs.readFileSync('./sslcert/privkey.pem')
  };
  // register a redirection middleware for HTTPS
  app.use(function requireHTTPS(req, res, next) {
    console.log("Automatic HTTP Redirection to ssl " + port);
    if (!req.secure) {
      return res.redirect('https://' + req.headers.host + req.url);
    }
    next();
  });

  https.createServer(options, app).listen(httpsPort, function () {
    console.log("HTTPS Server listening on port " + httpsPort);
  });

  app.listen(port, function () {
    console.log("HTTP Server listening on port " + port);
  });
} else {
  app.listen(port, function () {
    console.log("HTTP Server listening on port " + port);
  });
}
