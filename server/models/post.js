var mongoose = require('mongoose');

var postSchema = new mongoose.Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  title: {
    type: String
  },
  category: {
    type : String
  },
  cover: {
    type: String
  },
  body: {
    type: String
  },
  created_at: {
    type: Date,
    default: Date.now()
  },
  posted_on: {
    type: Date,
    default: null
  }
});

postSchema.set('toJSON', {
  transform: function(doc, ret) {
    delete ret.__v;
    return ret;
  }
});

var post = mongoose.model('post', postSchema);

module.exports = post;
