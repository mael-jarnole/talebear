/**
 * Created by Maël on 14/07/2017.
 */
import {Component, OnInit} from '@angular/core';
import {PostService} from '../commons/services/post.service';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {

  // TODO make a Ressource<T> class to type the responses from server
  articles;

  ngOnInit() {
  }

  constructor(private postService: PostService) {
  }

  getCategory(category: string) {
    this.postService.getPublishedPostsByCategory(category).subscribe(response => {
      this.postService.announceSharedPosts(response);
    }, error => {
      throw error;
    });
  }
}
