export class Resource<T> {
  resource: T;
  _links: ResourceLinks;
};

export class ResourceLinks extends Object {

  hasRelationship(rel) {
    return this.hasOwnProperty(rel);
  }

  getRelationship(rel) {
    return this.hasRelationship(rel) &&  this[rel]['href'] ? this[rel]['href'] : '';
  }

};
