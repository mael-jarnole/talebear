/**
 * Created by Maël on 31/03/2017.
 */
const gulp = require("gulp"); //http://gulpjs.com/
const util = require("gulp-util"); //https://github.com/gulpjs/gulp-util
const sass = require("gulp-sass"); //https://www.npmjs.org/package/gulp-sass
const autoprefixer = require('gulp-autoprefixer'); //https://www.npmjs.org/package/gulp-autoprefixer
const minifycss = require('gulp-minify-css'); //https://www.npmjs.org/package/gulp-minify-css
const rename = require('gulp-rename'); //https://www.npmjs.org/package/gulp-rename
const log = util.log;
