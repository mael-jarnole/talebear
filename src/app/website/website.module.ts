import {WebsiteRoutingModule} from "./website-routing.module";
import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {BundledMaterialModule} from "../cots/module/material.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {WebsiteComponent} from "./website.component";
import {ArticleTilesComponent} from "./components/article-tiles/article-tiles.component";
import {ArticleViewerComponent} from "./components/article-viewer/article-viewer.component";
import {CmsModule} from "../cms/cms.module";
import {SafeHtmlPipe} from "../commons/pipes/sanitize.pipe";
import {PostService} from "../commons/services/post.service";


@NgModule({
  declarations: [
    WebsiteComponent,
    ArticleTilesComponent,
    ArticleViewerComponent,
    SafeHtmlPipe
  ],
  imports: [
    CmsModule,
    HttpModule,
    BundledMaterialModule,
    BrowserAnimationsModule,
    WebsiteRoutingModule,
  ],
  providers: [
    PostService
  ]
})
export class WebsiteModule {
}
