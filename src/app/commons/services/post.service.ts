/**
 * Created by Maël on 21/05/2017.
 */
import {Injectable} from '@angular/core';
import {HttpHeaders, HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Subject} from 'rxjs/Subject';
import {Resource} from '../models/resource';

@Injectable()
export class PostService {

  private sharedPosts = new Subject<Resource<Post[]>>();

  sharedPostsAnnounced$: Observable<any> = this.sharedPosts.asObservable();

  constructor(private _http: HttpClient) {}

  announceSharedPosts(postsResource) {
    this.sharedPosts.next(postsResource);
  }


  private appendPostToFormData(post: Post, formData: FormData): void {
    for (const property in post) {
      if (post.hasOwnProperty(property) && property !== 'body') {
        formData.append(property, post[property]);
      } else if (property === 'body') {
        formData.append(property, post[property]);
      }
    }
  }

  getPosts(): Observable<any> {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this._http.get(environment.baseUrl + '/api/post', {headers});
  }

  getPostById(id: string): Observable<any> {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this._http.get(environment.baseUrl + '/api/post/' + id, {headers});
  }

  deletePostById(id: string): Observable<any> {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this._http.delete(environment.baseUrl + '/api/post/' + id, {headers});
  }

  getPublishedPosts(): Observable<any> {
    return this._http.get(environment.baseUrl + '/api/post/published');
  }

  getPublishedPostsByCategory(category: string): Observable<any> {
    const params = new HttpParams();
    params.set('category', category);
    return this._http.get(environment.baseUrl + '/api/post/published/', {
      params: params
    });
  }

  setReleaseDate(post: Post, date: number) {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    return this._http.patch(environment.baseUrl + '/api/post/' + post._id + '/' + date, date, {headers});
  }

  saveNewPost(post: Post, formData: FormData): Observable<any> {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    const fd = !formData ? new FormData() : formData;
    this.appendPostToFormData(post, fd);
    return this._http.post(environment.baseUrl + '/api/post', fd, {headers});
  }
}
;

export class Post {
  author: string;
  title: string;
  category: string;
  cover: string;
  body: string;
  created_at: Date;
  _id: string;
  posted_on: string;

  constructor(userId: string) {
    this.author = userId;
    this.title = '';
    this.category = '';
    this.cover = '';
    this.body = '';
    this._id = '';
    this.posted_on = '';
    this.created_at = new Date();
  }
}

;
