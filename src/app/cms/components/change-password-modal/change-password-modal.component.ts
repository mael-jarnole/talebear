import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UserService} from '../../../commons/services/user.service';

@Component({
  selector: 'app-change-password-modal',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.scss']
})
export class ChangePasswordModalComponent implements OnInit {

  oldPassword: string;
  confirmPassword: string;
  newPassword: string;
  userId: string;
  message: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<ChangePasswordModalComponent>,
              private userService: UserService) {
  }

  ngOnInit() {
    this.userId = this.data.toString();
  }

  changePassword() {
    if (this.oldPassword === this.confirmPassword) {
      this.userService.changePasswordById(
        {
          old: this.oldPassword,
          new: this.newPassword,
          userId: this.userId
        }
      ).subscribe(() => {
        this.closeModal('Mot de passe OK');
      }, (error) => {
        this.closeModal('Une erreur est survenue' + error);
      });
    } else {
      this.message = 'Les mots de passe doivent correspondre.';
    }
  }

  closeModal(message: string) {
    this.dialogRef.close(message);
  }

}
