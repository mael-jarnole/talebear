var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('user');

passport.use(new LocalStrategy({
    usernameField: 'email'
  },
  function(username, password, done) {
    User.findOne({ email: username }, function (err, user) {
      if (err) { return done(err); }

      if (!user) {
        return done(null, false, {
          message: 'Pas d\'utilisateur lié à l\'adresse mail: '+username
        });
      }
      // Return if password is wrong
      if (!user.validPassword(password)) {
        return done(null, false, {
          message: 'Le mot de passe est incorrect'
        });
      }
      // If credentials are correct, return the user object
      return done(null, user);
    });
  }
));
