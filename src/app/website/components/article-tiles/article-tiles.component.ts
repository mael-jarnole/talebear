import {Component, OnInit} from '@angular/core';
import {Post, PostService} from '../../../commons/services/post.service';
import {ActivatedRoute} from '@angular/router';
import {Http} from '@angular/http';
import {environment} from '../../../../environments/environment';
import {MessageComponent} from '../../../commons/components/message/message.component';
import {MatDialog} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {Resource} from '../../../commons/models/resource';

@Component({
  selector: 'app-article-tiles',
  templateUrl: './article-tiles.component.html',
  styleUrls: ['./article-tiles.component.scss']
})
export class ArticleTilesComponent implements OnInit {

  posts: Post[];
  firstRow: Post[];
  secondRow: Post[];
  links;
  subscription: Subscription;

  constructor(private route: ActivatedRoute, private _http: Http,
              private dialog: MatDialog, private postService: PostService) {
    this.subscription = postService.sharedPostsAnnounced$.subscribe(
      (sharedPosts: Resource<Post[]>) => {
        this.posts = sharedPosts.resource;
        this.firstRow = this.posts.slice(0, 3);
        this.secondRow = this.posts.slice(3, 5);
        this.links = sharedPosts._links;
      });
  }

  ngOnInit() {
    this.route.data.subscribe((data: {posts: Resource<Post[]>}) => {
      console.log(data);
      this.posts = data.posts.resource;
      this.firstRow = this.posts.slice(0, 3);
      this.secondRow = this.posts.slice(3, 5);
      this.links = data.posts._links;
    }, error => {
      this.dialog.open(MessageComponent, {data: JSON.stringify(error)});
    });
  }

  loadPageContent(next: boolean) {
    this._http.get(environment.baseUrl + (next ? this.links.next.href : this.links.previous.href))
      .map(res => res.json()).subscribe((res: Resource<Post[]>) => {
      this.posts = res.resource;
      this.firstRow = this.posts.slice(0, 3);
      this.secondRow = this.posts.slice(3, 5);
      this.links = res._links;
    });
  }

}
