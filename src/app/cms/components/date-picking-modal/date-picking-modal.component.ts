import {Component, Inject, OnInit} from '@angular/core';
import {Post, PostService} from '../../../commons/services/post.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-date-picking-modal',
  templateUrl: './date-picking-modal.component.html',
  styleUrls: ['./date-picking-modal.component.css']
})
export class DatePickingModalComponent implements OnInit {

  date: string;
  post: Post;

  constructor(private postService: PostService, private dialogRef: MatDialogRef<DatePickingModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.post = this.data.post;
  }

  releasePost() {
    const d = new Date(this.date);
    this.postService.setReleaseDate(this.post, + d).subscribe(() => {
      this.dialogRef.close();
    }, error => {
      this.dialogRef.close(error);
    });
  }

}
