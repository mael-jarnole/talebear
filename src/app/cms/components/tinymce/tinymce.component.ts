import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange
} from "@angular/core";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-tinymce',
  templateUrl: './tinymce.component.html',
  styleUrls: ['./tinymce.component.css']
})
export class TinymceComponent implements AfterViewInit, OnDestroy, OnInit, OnChanges {

  @Input() elementId: String;
  @Input() form: FormGroup;
  @Output() onEditorKeyUp = new EventEmitter<any>();

  editor;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes.hasOwnProperty('form')) {
      if (this.editor && this.form.get('body') !== null) {
        this.editor.setContent(this.form.get('body').value);
      }
    }
  }

  ngAfterViewInit() {
    this.initTinyMce();
  }

  private initTinyMce() {
    tinymce.init({
      selector: '#' + this.elementId,
      plugins: ['link', 'paste', 'table', 'image imagetools'],
      language: 'fr_FR',
      language_url: '/assets/langs/fr_FR.js',
      images_upload_url: '/upload/content',
      images_upload_credentials: true,
      paste_data_images: true,
      image_caption: true,
      skin_url: '/assets/skins/lightgray',
      height: 500,
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          this.onEditorKeyUp.emit(content);
          this.form.get('body').setValue(content);
        });
        editor.on('change', () => {
          const content = editor.getContent();
          this.onEditorKeyUp.emit(content);
          this.form.get('body').setValue(content);
        });
      },
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }
}
