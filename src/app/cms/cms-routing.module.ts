// app.routes.ts
import {NgModule} from "@angular/core";

import {LoginComponent} from "./components/login/login.component";
import {ProfileComponent} from "./components/profile/profile.component";
import {HomeComponent} from "./components/home/home.component";
import {UserManagementComponent} from "./components/userManagement/userManagement.component";
import {EditorComponent} from "./components/editor/editor.component";
import {CheckerComponent} from "./components/checker/checker.component";
import {RouterModule, Routes} from "@angular/router";
import {UserResolver} from "../commons/services/user-resolver.service";
import {CmsComponent} from "./cms.component";
import {AuthGuard} from "../commons/services/auth-guard.service";
import {AuthenticatorService} from "../commons/services/authenticator.service";
import {PublisherComponent} from "./components/publisher/publisher.component";
import {PostResolverService} from "../commons/services/post-resolver.service";

const AppRoutes: Routes = [
  {
    path: 'cms',
    component: CmsComponent,
    resolve: {user: UserResolver},
    canActivate: [AuthGuard],
    children : [{
      path: '',
      children: [
        { path: '', component: HomeComponent },
        { path: 'profile', component: ProfileComponent },
        { path: 'user-management', component: UserManagementComponent },
        { path: 'editor/:userId', component:  EditorComponent},
        { path: 'checker', component: CheckerComponent, resolve: {posts : PostResolverService}},
        { path: 'checker/:postId', component: CheckerComponent, resolve: {posts: PostResolverService, currentPost: PostResolverService}},
        { path: 'publisher', component: PublisherComponent, resolve: {posts : PostResolverService}}
      ]
    }]
  }, {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(AppRoutes),
  ],
  exports: [
    RouterModule
  ],
  providers: [
    UserResolver,
    PostResolverService,
    AuthGuard,
    AuthenticatorService
  ]
})
export class CmsRoutingModule {}
