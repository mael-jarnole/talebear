import { Injectable} from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot } from '@angular/router';
import { UserService, User } from './user.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class UserResolver implements Resolve<User> {
  constructor(private userService: UserService, private router: Router) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    return this.userService.getCurrentUserInfo().map(user => {
      if (user) {

        return user;
      } else {
        this.router.navigate(['/login']).then();
        return null;
      }
    });
  }
}
