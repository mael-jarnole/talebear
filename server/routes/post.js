const express = require('express');
const postCtrl = require('../controllers/post');
const router = express.Router();
const jwt = require('express-jwt');
const conf = require('../conf')
// auth middleware
const auth = jwt({
  secret: conf.secret
});

router.get('/published', (req, res) => postCtrl.getAllPostsPublished(req, res));

router.get('/', auth, (req, res) => postCtrl.getAllPosts(req, res));

router.post('/', auth, (req, res) => postCtrl.createPost(req,res));

router.put('/:id', auth, (req, res) => postCtrl.updatePost(req,res));

/**
 * Sets the release date of an article
 */
router.patch('/:id/:date', auth, (req, res) => postCtrl.setDate(req, res));
/**
 * Updates the cover image of an article
 */
router.patch('/:id/attributes/cover/', auth, (req, res) => postCtrl.setCover(req, res));

router.delete('/:id', auth, (req, res) => postCtrl.deletePost(req,res));

router.get('/:id', (req, res) => postCtrl.getPostById(req,res));

router.get('/:category', auth, (req, res) => postCtrl.getAllPostsByCategory(req,res));

module.exports = router;

