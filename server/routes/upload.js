const express = require('express');
const router = express.Router();
const jwt = require('express-jwt');
const conf = require('../conf');
const multer  = require('multer');
const upload = multer({dest: './uploads/'}).single('avatar');
const cors = require('cors');
const cloudinary = require('cloudinary');
rmdir = require('rimraf');

const cleanUploadDirectory = function () {
  rmdir('./uploads/', function (error) {
    console.log(error);
  });
}

// auth middleware
var auth = jwt({
  secret: conf.secret
});

router.options('/avatar', cors());
router.post('/avatar',cors(), auth , (req, res) => {
  upload(req, res, function (err) {
    if (err) {
      return res.status(422).json(err);
    }
    cloudinary.uploader.upload(req.file.path,function (result) {
      return res.status(200).json({avatar: result.secure_url});
    }, {eager: [{width: 150, height: 150, crop: 'thumb', format: "png"},{gravity: 'face', radius: 'max', format: "png"}]});
  });
});

router.options('/content', cors());
router.post('/content', cors(), (req, res) => {
  const uploadFile = multer({dest: './uploads/'}).single('file');
  uploadFile(req, res, function (err) {
    if (err) {
      return res.status(422).json(err);
    }
    cloudinary.uploader.upload(req.file.path,function (result) {
      return res.status(200).json({location: result.secure_url});
    });
  });
});

router.options('/done', cors());
router.post('/done',cors(), auth , (req, res) => {
  cleanUploadDirectory();
  res.end();
});

module.exports = router;
