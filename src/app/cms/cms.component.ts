import {Component, OnInit} from '@angular/core';
import {AuthenticatorService} from '../commons/services/authenticator.service';
import {AppService} from '../commons/services/app.service';
import {User} from '../commons/services/user.service';
import {
  ActivatedRoute,
  Event as RouterEvent,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router
} from '@angular/router';

@Component({
  selector: 'app-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.css']
})
export class CmsComponent implements  OnInit {
  private roles: string[];
  currentUser: User;
  loading = true;

  ngOnInit () {
    // get Frontend configuration
    this.App.getConf().subscribe( ( res: any ) => {
      this.roles = (res.roles ? res.roles : ['Definir les roles dans la configuration...']) ;
      this.App.setRoles(this.roles);
    });
    this.route.data.subscribe((data: { user: User }) => {
      this.currentUser = data.user;
    }, error => {
      window.alert(JSON.stringify(error));
    });
  }

  constructor(private auth: AuthenticatorService, private router: Router,
              private App: AppService, private route: ActivatedRoute) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    });
  }

  // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true
    }
    if (event instanceof NavigationEnd) {
      this.loading = false
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loading = false
    }
    if (event instanceof NavigationError) {
      this.loading = false
    }
  }

  loggedIn() {
    return this.auth.isLoggedIn();
  }

  logout () {
    this.auth.logout();
    this.router.navigate(['/login']).then(function () {});
  }
}
