const UserController = require('../controllers/user');
const express = require('express');
const router = express.Router();
const conf = require('../conf')
const jwt = require('express-jwt');
// auth middleware
const auth = jwt({
  secret: conf.secret
});


const handleRequestError = function (err, req, res, next) {
    if (err){
      res.status(500).json(err);
    }
    else {
      next();
    }
};

/**
 * getCurrentUser info by token
  */
router.get('/', auth, (err, req, res, next) => handleRequestError(err, req, res, next),
  (req,res) => UserController.getCurrentUserInfo(req,res));

/**
 * register new user
 */
router.put('/', auth, (err, req, res, next) => handleRequestError(err, req, res, next),
  (req,res) => UserController.updateUser(req,res));

/**
 * Change the password of an existing user
 */
router.patch('/:userId/password', auth, (err, req, res, next) => handleRequestError(err, req, res, next),
  (req,res) => UserController.changePassword(req,res));
/**
 *
 */
router.delete('/', auth, (err, req, res, next) => handleRequestError(err, req, res, next),
  (req,res) => UserController.deleteUser(req,res));

  /**
   * get all users info
   **/
  router.get('/all', auth, (err, req, res, next) => handleRequestError(err, req, res, next),
    (req,res) => UserController.getAllUsers(req,res));

module.exports = router;
