import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-cover-image-modal',
  templateUrl: './cover-image-modal.component.html',
  styleUrls: ['./cover-image-modal.component.scss']
})
export class CoverImageModalComponent implements OnInit {

  /** Attributes **/
  postId: string;
  currentCover: string;
  modalResponse;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.postId = this.data.postId;
    this.currentCover = this.data.cover;
  }

  // TODO implement file upload
  onUploadOutput(event: any): void {
  }

}

