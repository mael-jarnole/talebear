import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Post, PostService} from './post.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class PostResolverService implements Resolve<Post[] | Post> {
  constructor(private postService: PostService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Post[] | Post> {
    if (state.url.includes('/webzine')) {
      if (route.params.id) {
        return this.postService.getPostById(route.params.id).map(post => {
          return [post];
        });
      } else {
        return this.postService.getPublishedPosts().map(posts => {
          return posts;
        });
      }
    } else {
      return this.postService.getPosts().map(posts => {
        return posts;
      });
    }
  }
}
