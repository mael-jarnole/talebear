import { TalebearPage } from './app.po';

describe('talebear App', () => {
  let page: TalebearPage;

  beforeEach(() => {
    page = new TalebearPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
