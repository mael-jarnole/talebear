const Post = require('../models/post');
const multer  = require('multer');
const upload = multer(
  {
    storage: multer.diskStorage({
      destination: './uploads/',
      filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
      }
    })
  }).single('cover');
const cloudinary = require('cloudinary');
const hal = require('hal');

/** CONSTANTS **/
 const COUNT_BY_PAGE = 5;

/**
 * @private
 */
function _createPost (req,res) {
  Post.count({_id:  req.body._id ? req.body._id : req.params.id}, function (err, count){
    if(count>0){
      _updatePost(req,res);
    } else {
      Post.create({
        author : req.body.author,
        title : req.body.title,
        body: req.body.body,
        category: req.body.category
      }, function (err, post) {
        if (!err) {
          res.status(200);
          res.json(post);
        } else {
          res.status(500);
          res.json(err);
        }
      });
    }
  });
}

function _updatePost(req,res) {
  const _id = req.params.id ? req.params.id : req.body._id;
  Post.findByIdAndUpdate(_id,
    {
      title : req.body.title,
      body: req.body.body,
      category: req.body.category
    }, {new : true}, function (err, post) {
      if (!err) {
        res.status(200);
        res.json(post);
      } else {
        res.status(500);
        res.json(err);
      }
    });
}

module.exports.getAllPosts = function (req,res) {
  Post.count({}, function(err, count){
    if(err) {
      res.status(503).json(err);
    }
    let currentPage = req.query.page;
    currentPage = currentPage ? currentPage : 0;
    let query = Post.find({});
    query.skip(COUNT_BY_PAGE * currentPage);
    query.limit(COUNT_BY_PAGE);
    query.sort({created_at: 'ascending'});
    query.select({body: 0});
    query.exec((err,posts)=>{
      if(err) {
        res.status(503).json(err);
      } else {
        currentPage = parseInt(currentPage, 10);
        const resource = new hal.Resource({post: posts, total: count, currentPage: currentPage}, req.originalUrl);
        if (currentPage * COUNT_BY_PAGE + COUNT_BY_PAGE < count) {
          const nextPage = currentPage + 1;
          resource.link("next", req.originalUrl.split('?')[0]+"?page="+nextPage);
        }
        if (currentPage > 0) {
          const previousPage = currentPage - 1;
          currentPage = parseInt(currentPage, 10);
          currentPage--;
          resource.link("previous", req.originalUrl.split('?')[0]+"?page="+previousPage);
        }
        res.status(200).json(resource);
      }
    }).then();
  });
}

/**
 * TODO
 * @param req
 *    req.params: {
 *      start: 0
 *      end: 10
 *    }
 * @param res
 * @return {
 *  posts: [],
 *  total: (number),
 *  links: {
 *    previous:
 *    next:
 *  }
 * }
 */
module.exports.getAllPostsPublished = function (req,res) {
  Post.count({}, function(err, count){
    if(err) {
      res.status(503).json(err);
    }
    let currentPage = req.query.page;
    currentPage = currentPage ? currentPage : 0;
    let query = Post.find({
      'posted_on' : {$ne : null, $lte: Date.now()},
      'category': req.query.category ? req.query.category : {$ne : null}
      }, '_id title posted_on cover category');
    query.skip(COUNT_BY_PAGE * currentPage);
    query.limit(COUNT_BY_PAGE);
    query.sort({created_at: 'ascending'});
    query.exec((err,posts)=>{
      if(err) {
        res.status(503).json(err);
      } else {
        currentPage = parseInt(currentPage, 10);
        const resource = new hal.Resource({resource: posts}, req.originalUrl);
        if (currentPage * COUNT_BY_PAGE + COUNT_BY_PAGE < count) {
          const nextPage = currentPage + 1;
          resource.link("next", req.originalUrl.split('?')[0]+"?page="+nextPage
            + (req.query.category ? "&category="+encodeURIComponent(req.query.category) : ""));
        }
        if (currentPage > 0) {
          const previousPage = currentPage - 1;
          currentPage = parseInt(currentPage, 10);
          currentPage--;
          resource.link("previous", req.originalUrl.split('?')[0]+"?page="+previousPage
            + (req.query.category ? "&category="+encodeURIComponent(req.query.category) : ""));
        }
        res.status(200).json(resource);
      }
    }).then();
  });
}

module.exports.getAllPostsByCategory = function (req,res) {
  let query = Post.find({category: req.params.category});
  query.sort({created_at: 'descending'});
  query.exec((err,posts)=>{
    if(err) {
      res.status(400).json(err);
    } else {
      res.status(200).json(posts);
    }
  });
}

module.exports.createPost = function (req,res) {
  upload(req, res, function (err) {
    if (err) {
      return res.status(500).json(err);
    }
    if (req.file || req.files) {
      cloudinary.uploader.upload(req.file.path, function (result) {
        _createPost(req,res,result.secure_url);
      });
    } else {
      _createPost(req,res);
    }
  });
}

module.exports.getPostById = function (req,res) {
 let query = Post.findById(req.params.id);
 query.populate({
   path: 'author',
   select: '-_id -hash -salt -post'
});
 query.exec((err, post) => {
   if (!err) {
     res.status(200);
     res.json(post);
   } else {
     res.status(500);
     res.json(err);
   }
 }).then();
}

module.exports.deletePost = function (req,res) {

  Post.findByIdAndRemove(req.params.id, function (err, post) {
    if (!err) {
      res.status(200);
      res.json(post);
    } else {
      res.status(500);
      res.json(err);
    }
  });
}

module.exports.updatePost = function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.status(500).json(err);
    }
    if (req.file) {
      cloudinary.uploader.upload(req.file.path, function (result) {
        _updatePost(req,res,result.secure_url);
      });
    } else {
      _updatePost(req,res);
    }
  });
}

module.exports.setDate = function(req,res) {
  Post.findByIdAndUpdate(req.params.id, {posted_on : req.params.date}, function (err, post) {
    if (!err) {
      res.status(204);
      res.json(post);
    } else {
      res.status(500);
      res.json(err);
    }
  });
}

module.exports.setCover = function(req,res) {
  upload(req, res, function (err) {
    if (err) {
      return res.status(500).json(err);
    }
    if (req.file || req.files) {
      cloudinary.uploader.upload(req.file.path, function (result) {
        const _id = req.params.id ? req.params.id : req.body._id;
        Post.findByIdAndUpdate(_id,
          {
            cover: result.secure_url ? result.secure_url : ""
          }, {new : true}, function (err, post) {
            if (!err) {
              res.status(200);
              res.json(post);
            } else {
              res.status(500);
              res.json(err);
            }
          });
      });
    } else {
      return res.status(404).json(new Error('File not found.'));
    }
  });
}
