import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AppComponent} from "./app.component";
import {CmsModule} from "./cms/cms.module";
import "hammerjs";
import {PageNotFoundComponent} from "app/commons/components/not-found.component";
import {AppRoutingModule} from "./app-routing.module";
import {WebsiteModule} from "./website/website.module";
import {BrowserModule} from "@angular/platform-browser";
import {MessageComponent} from "./commons/components/message/message.component";
import {BundledMaterialModule} from "./cots/module/material.module";

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent, MessageComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'universal' }),
    CommonModule,
    CmsModule,
    WebsiteModule,
    AppRoutingModule,
    BundledMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    MessageComponent
  ]
})
export class AppModule { }
