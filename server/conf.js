module.exports = {
  db_url : process.env.MONGODB || 'localhost:27017',
  secret : process.env.SECRET,
  domain : process.env.DOMAIN,
  frontend : {
    roles: [
      'Admin',
      'Correcteur',
      'Posteur',
      'Responsable de publication'
    ]
  }
}
