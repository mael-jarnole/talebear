import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {Post, PostService} from '../../../commons/services/post.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {CoverImageModalComponent} from '../cover-image-modal/cover-image-modal.component';
import {MessageComponent} from '../../../commons/components/message/message.component';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['editor.component.css'],
})
export class EditorComponent implements OnInit {
  userId: string;
  postForm: FormGroup;
  bodyForm: FormGroup;
  currentPost: Post;
  private input: boolean;
  categories = ['Bande dessinée', 'Vie de l\'association'];
  @Output() onPostModification = new EventEmitter<Post>();
  formData: FormData;
  private isInCheckerMode: boolean;

  @Input() set postInput(postInput: Post) {
    this.postForm = this.fb.group(postInput);
    this.input = true;
    this.currentPost = postInput;
    this.bodyForm = this.fb.group({
      body: [this.postForm.get('body').value]
    });
    this.postForm.removeControl('body');
    this.postForm.addControl('body', this.bodyForm);
  };

  constructor(private route: ActivatedRoute, private title: Title,
              private postService: PostService, private fb: FormBuilder, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.snapshot.url.forEach(segment => {
      if (segment.path === 'checker') {
        this.isInCheckerMode = true;
      }
    });
    this.userId = this.route.snapshot.params['userId'];
    this.title.setTitle('Édition d\'un article');
    if (!this.input) {
      const post = new Post(this.userId);
      this.createPostInEditor(post);
    }
  }

  private createPostInEditor(post: Post) {
    this.postForm = this.fb.group(post);
    this.postForm.patchValue({'category': post.category && post.category.length ? post.category : this.categories[0]});
    this.bodyForm = this.fb.group({
      body: [post.body]
    });
    this.postForm.removeControl('body');
    this.postForm.addControl('body', this.bodyForm);
  }

  deletePost() {
    this.postService.deletePostById(this.currentPost._id).subscribe(post => {
      this.dialog.open(MessageComponent,  { data: 'Supression de ' + post.title + ' OK' });
      this.postForm.reset();
      this.onPostModification.emit(post);
    }, error => {
      this.dialog.open(MessageComponent,  { data: 'Erreur:\n' + error });
    });
  }

  openCoverModal () {
    const ref = this.dialog.open(CoverImageModalComponent,
      {
        data : {
          postId : this.currentPost._id,
          cover: this.currentPost.cover
        }
      });
    ref.afterClosed().subscribe(() => {
      if (ref.componentInstance.modalResponse) {
        this.currentPost = ref.componentInstance.modalResponse;
      }
      this.onPostModification.emit(this.currentPost);
    });
  }

  savePost() {
    this.postForm.removeControl('body');
    const bodyControl = this.fb.control(this.bodyForm.get('body').value);
    this.postForm.addControl('body', bodyControl);
    this.postService.saveNewPost(this.postForm.value, this.formData).subscribe((post: Post) => {
      this.dialog.open(MessageComponent,  { data: 'Sauvegarde de ' + post.title + ' OK \n'});
      if (this.isInCheckerMode) {
        this.onPostModification.emit(post);
      } else {
        this.currentPost = post;
        this.createPostInEditor(this.currentPost);
      }
    }, (error) => {
      this.dialog.open(MessageComponent,  { data: 'Une erreur est survenue \n' + error });
    })
  }

  keyupHandlerFunction($event) {

  }
}
