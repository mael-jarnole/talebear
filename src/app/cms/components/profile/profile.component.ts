/**
 * Created by Maël on 27/03/2017.
 */
// login.component.ts
import { Component, OnInit } from '@angular/core';

import {UserService, User} from '../../../commons/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {

  user: User;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getCurrentUserInfo().subscribe((user) => {
      this.user = user;
    });
  }
}
